# README #
Bu README pdf de olan melumatlari pars ederek excell filena yazir.

### Bu project ne is gorur? ###

* Bu project pdf de olan melumatlari pars ederek excell filena yazir.
* [pdf_parser_new]( https://cavidanh@bitbucket.org/cavidanh/pdf_parser_new.git)

### Hansi texnologiyalardan istifade olur? ###

* Pdfminer

* openpyxl

* BeautifulSoup

### How do I get set up? ###

1) Bu projectin meqsedi gelen pdfleri pars ederecek excell foldere yazmaqdi.Bunu etmek ucun pdfden gelen datalari 1 ci novbede xml formatina ceviririk.
* Location:(pdf_parser/parser_app/pdf_to_xml_and_excell_parser/pdf_to_xml_convertor.py)
2)Xml formata cevirende her bir sirket pdfnin datalari muxtelif formatda geldiyi ucun onlarin her birine ayri parser yazilmisdir.
* Location: (pdf_parser/parser_app/pdf_to_xml_and_excell_parser/xml_parser.py)
3)Xml formati pars edikden sonra excell file standart basliqlar altinda qeyd edirik.
* Location: (pdf_parser/parser_app/pdf_to_xml_and_excell_parser/main.py)
4)Bu emeliyyatlar icra olunduqdan sonra upload olunur.
* Location: (pdf_parser/parser_app/views.py)

### Yeni parserleri yazmaq ucun asagidaki emeliyyatlari icra etmek lazimdir: ###

1) Bu pathdeki file a daxil oluruq (pdf_parser/parser_app/pdf_to_xml_and_excell_parser/xml_parser.py).Burdaki bir funksiyanin documentasiyasini oxuyuruq.(Ex:koton_xml_parser()) Oxuduqdan sonra onu copy edirik ve qeyd olunmus hissleri documentasiyada qeyd olunan formada deyisdiririk.
2)Sonra bu pathdeki file a daxil oluruq (pdf_parser/parser_app/pdf_to_xml_and_excell_parser/main.py).Buda iki hissede artim olacaq:

1) xml_to_xls() bu funksiyanin daxilinde qeyd olunan formada artirdigimiz pdf e uygun qeydler apaririq.

2) Asagida commentleri diqqetle oxuduqda gore bilerikki excell foldere yazan codelar qeyd olunub onlarida artirmaq istediyimiz pdf e uygun qeyd etdikden sonra codemuzu isledirik.

### Ne kimi problem cixa biler ve bu zaman ne ede bilerik? ###

Birinci novbede mutleq ve mutleq xml filelari duzgun analiz etmeliyik ordaki cordinatlarin yanlis qeyd olunmasi codemuzda ciddi fesadlar dogura biler.

Ikinci novbede xml_parser.py in icinde code elave etdikde qeyd olunan qaydalara uygun gedin cunki bu projectin esas hissesi ordadi.Eks teqdirde yaranan problemlere bunu yazan proqramist mesuliyyat dasimir.Cunki bu project uzun arasdirmalar esnasinda yazilib ve bunu yazan proqramist bu emeliyyatlar esasinda gederek 1 den cox parser yazib.

Uchuncu novbede main.py in icinde excelle yazmaq ucun istifade etdiyimiz codelarda problem cixa biler cunki datalarin rowlari bir birine uygun olmaya biler bunun ucun ise gelen datalarla excelle yazan code uygunlardirmaq lazimdir main.py in icinde size lazim olan her bir resurs vardir.

### Bu projectde nezerde tutulmayan problemler cixdiqda ne edek ###

Bu email vasitesi ile nu projecti yazan proqramistle elaqe yaratmaq olar: cavidan.mahmudoglu@gmail.com

