from django.urls import path
from parser_app.views import upload

urlpatterns = [
    path('', upload, name='upload')
]