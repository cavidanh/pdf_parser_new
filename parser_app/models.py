from django.db import models

# Create your models here.

class TemporaryFiles(models.Model):
    token = models.CharField(max_length=255)
    image = models.ImageField(upload_to='test')

    def __str__(self):
        return f"{self.token}"