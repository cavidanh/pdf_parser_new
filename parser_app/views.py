from django.shortcuts import render
from django.http import FileResponse, JsonResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from .pdf_to_xml_and_excell_parser import pdf_to_xml_convertor, xml_parser, main
from .models import TemporaryFiles
import os
import uuid
import zipfile
import string
import random


def random_path_generator(size=20, chars=string.ascii_letters+ string.digits):
    return "".join([random.choice(chars) for _ in range(size)])


# Create your views here.
def upload(request):
    '''
    Bu funksiya parser funksiyalarini(from .pdf_to_xml_and_excell_parser import pdf_to_xml_convertor, xml_parser, main) ve
    templateden gelen pdfleri goturub
    icinde islederek upload emeliyyatini yerine yetirir. 
    Sonda ise outputdan gelen excell filelarini goturub zip file a  universal adla qeyd edir.Ve download edir.
    '''
    path = ''
    if request.method == "GET":
        path = random_path_generator()

    if request.method == 'POST' and 'file' in request.FILES:
        TemporaryFiles.objects.create(
            token=request.POST.get("token"),
            image=request.FILES['file']
        )
        return JsonResponse({"uploaded":True})
    elif "complete" in request.POST:
        xls_files = []
        myfiles = [item.image for item in TemporaryFiles.objects.filter(token=request.POST.get('token'))]
        print(myfiles)
        for myfile in myfiles:
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
        
            xml = pdf_to_xml_convertor.convert(os.path.join(fs.base_location, filename))
            workbook = main.xml_to_xls(xml)
            file_tmp_name = f"/tmp/{uuid.uuid4()}.xls"
            workbook.save(file_tmp_name)
            xls_files.append(file_tmp_name)

        zip_name = f"{uuid.uuid4()}.zip"
        zip_tmp_name = f"/tmp/{zip_name}"
        zf = zipfile.ZipFile(zip_tmp_name, "w")
        for filename in xls_files:
            zf.write(filename)
        zf.close()
        
        return FileResponse(open(zip_tmp_name, "rb"), filename=zip_name, as_attachment=True)
    return render(request,'upload.html', {'path':path})
