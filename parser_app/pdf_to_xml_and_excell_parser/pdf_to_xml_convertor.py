try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import XMLConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import os
import sys, getopt
from os import listdir
from io import BytesIO
from os.path import isfile, join
import io

BASE_DIR = os.getcwd()
def convert(fname, pages=None):
    '''
    Bu funksiya pdf fileni parse edir ve return edir.
    '''
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)
        
    rsrcmgr = PDFResourceManager()
    retstr = BytesIO()
    laparams = LAParams(char_margin=30.0)
    codec='utf-8'
    converter = XMLConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, converter)
    print(fname)

    file_type = fname.split(".")[-1]

    if file_type == "pdf" or file_type == "PDF":
        infile = open(fname, 'rb')
        for page in PDFPage.get_pages(infile, pagenums):
            interpreter.process_page(page)
        infile.close()
        converter.close()
        text = retstr.getvalue().decode()
        retstr.close
        return text