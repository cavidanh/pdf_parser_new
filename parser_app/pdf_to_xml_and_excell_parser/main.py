from bs4 import BeautifulSoup
import os
from os import listdir
from os.path import isfile, join
import io
from openpyxl import Workbook
import openpyxl
from parser_app.pdf_to_xml_and_excell_parser.xml_parser import koton_xml_parser, polo_xml_parser, defacto_xml_parser,morhipo_xml_parser,hm_xml_parser,kigili_xml_parser,PR_xml_parser

BASE_DIR = os.getcwd()

def xml_to_xls(handler):#xml den lazim olan datalari pars edib excell filena yazir.
    '''
    Bu funksiya 1 emeliyyat yerine yetirir:

    1) Basdan bize gelen her bir xml file in textini cixardiriq ve

        uygun parserleri yoxlanis esasinda qeyd edirik ve lazim olan datalari pars edirik.
    '''
    soup = BeautifulSoup(handler,'xml')
    text = "".join([x.text for x in soup.find_all('text') if x.text != '\n']) # gelen xml filelari textini verir bizde icinde vkn ve yaxudda ferqlilik  axtararaq uygun parserleri  qeyd edirik.
    if "5810590984" in text: #koton
        response_data = koton_xml_parser(handler)
    if "http://tr.uspoloassn.com" in text: #polo
        response_data = polo_xml_parser(handler)
    if "6500034716" in text: #defacto
        response_data = defacto_xml_parser(handler)
    if "1950252764" in text: #morhipo
        response_data =  morhipo_xml_parser(handler)
    if "4540511083" in text: #HM
        response_data = hm_xml_parser(handler)
    if "5620035661" in text: #kigili
        response_data = kigili_xml_parser(handler)
    if "http://www.pierrecardin.com.tr" in text: # pierrecardin
        response_data = PR_xml_parser(handler)
    
    workbook = Workbook()
    sheet = workbook.active

    sheet['A1']='Alış Faturasının Tarihi'
    sheet['B1']='Alış Faturasının Serisi'
    sheet['C1']='Alış Faturasının Nosu' 
    sheet['D1']='Satıcının Adı-Soyadı/Ünvanın' 
    sheet['E1']='Satıcının Vergi Kimlik Numarası/TC Kimlik Numarası'
    sheet['F1']='Alınan Mal veya Hizmetin Adi'
    sheet['G1']='Alınan Mal veya Hizmetin Miktarı' 
    sheet['H1']='Alış Faturasının KDV Hariç Tutarı'
    sheet['I1']='Alış Faturasının KDVsi'
    sheet['J1']='Bünyeye Giren Mal veya Hizmetin KDVsi'
    sheet['K1']='GGB Tescili Nosu (Alış İthalat ise)'
    sheet['L1']='Belgeye İlişkin İade Hakkl Doğuran İşlem Türü'
    sheet['M1']='Belgenin İndirime Konu Edildiği KDV Dönemi'
    sheet['N1']='Belgenin Yüklenildiği KDV Dönemi'

# Asagidaki codelarda ise pars olunmus melumatlari excell filellara yazir.
    if "5810590984" in text: #koton
        for key in response_data:
            if key == "title":
                sheet.cell(row=2,column=4).value = response_data[key]
            elif key == "vkn":
                sheet.cell(row=2,column=5).value = response_data[key] +"/" + response_data["tckn"]
            elif key == "tarih":
                sheet.cell(row=2,column=1).value = response_data[key]
            elif key == "fatura_Serisi":
                sheet.cell(row=2,column=2).value = response_data[key]
            elif key == "fatura_No":
                sheet.cell(row=2,column=3).value = response_data[key]
            elif "product" in key:
                line = int(key.split("_")[-1])
                product_list = response_data[key]
                sheet.cell(row=line+1,column=6).value = product_list[1]
                sheet.cell(row=line+1,column=7).value = product_list[2]
                sheet.cell(row=line+1,column=8).value = product_list[3]
                sheet.cell(row=line+1,column=9).value = product_list[4]
                sheet.cell(row=line+1,column=10).value = product_list[5]

    if "http://tr.uspoloassn.com" in  text or "http;//tr.uspoloassn.com" in text: #polo
        for key in response_data:
            if key == "title":
                sheet.cell(row=2,column=4).value = response_data[key]
            elif key == "vkn":
                sheet.cell(row=2,column=5).value = response_data[key]
            elif key == "tarih":
                sheet.cell(row=2,column=1).value = response_data[key]
            elif key == "fatura_Serisi":
                sheet.cell(row=2,column=2).value = response_data[key]
            elif key == "fatura_No":
                sheet.cell(row=2,column=3).value = response_data[key]
            elif "product" in key:
                line = int(key.split("_")[-1])
                product_list = response_data[key]
                sheet.cell(row=line+1,column=6).value = product_list[1]
                sheet.cell(row=line+1,column=7).value = product_list[3]
                sheet.cell(row=line+1,column=8).value = product_list[4]
            
    if "6500034716" in text: #defacto
        for key in response_data:
            if key == "title":
                sheet.cell(row=2,column=4).value = response_data[key]
            elif key == "vkn":
                sheet.cell(row=2,column=5).value = response_data[key] +"/" + response_data["tckn"]
            elif key == "tarih":
                sheet.cell(row=2,column=1).value = response_data[key]
            elif key == "fatura_Serisi":
                sheet.cell(row=2,column=2).value = response_data[key]
            elif key == "fatura_No":
                sheet.cell(row=2,column=3).value = response_data[key]
            elif "product" in key:
                line = int(key.split("_")[-1])
                product_list = response_data[key]
                sheet.cell(row=line+1,column=6).value = product_list[1]
                sheet.cell(row=line+1,column=7).value = product_list[2]
                sheet.cell(row=line+1,column=8).value = product_list[3]
                sheet.cell(row=line+1,column=9).value = product_list[4]
                sheet.cell(row=line+1,column=10).value = product_list[5]    

    if "1950252764" in text: #morhipo
        for key in response_data:
            if key == "title":
                sheet.cell(row=2,column=4).value = response_data[key]
            elif key == "vkn":
                sheet.cell(row=2,column=5).value = response_data[key]
            elif key == "tarih":
                sheet.cell(row=2,column=1).value = response_data[key]
            elif key == "fatura_Serisi":
                sheet.cell(row=2,column=2).value = response_data[key]
            elif key == "fatura_No":
                sheet.cell(row=2,column=3).value = response_data[key]
            elif "product" in key:
                line = int(key.split("_")[-1])
                product_list = response_data[key]
                sheet.cell(row=line+1,column=6).value = product_list[4]
                sheet.cell(row=line+1,column=7).value = product_list[0]
                sheet.cell(row=line+1,column=8).value = product_list[1]
                sheet.cell(row=line+1,column=9).value = product_list[2]

    if "4540511083" in text: #HM
        for key in response_data:
            if key == "title":
                sheet.cell(row=2,column=4).value = response_data[key]
            elif key == "vkn":
                sheet.cell(row=2,column=5).value = response_data[key] +"/" + response_data["tckn"]
            elif key == "tarih":
                sheet.cell(row=2,column=1).value = response_data[key]
            elif key == "fatura_Serisi":
                sheet.cell(row=2,column=2).value = response_data[key]
            elif key == "fatura_No":
                sheet.cell(row=2,column=3).value = response_data[key]
            elif "product" in key:
                line = int(key.split("_")[-1])
                product_list = response_data[key]
                sheet.cell(row=line+1,column=6).value = product_list[2]
                sheet.cell(row=line+1,column=7).value = product_list[4]
                sheet.cell(row=line+1,column=8).value = product_list[5]
                sheet.cell(row=line+1,column=9).value = product_list[7]
                sheet.cell(row=line+1,column=10).value = product_list[8]

    if "5620035661" in text: #kigili
        for key in response_data:
            if key == "title":
                sheet.cell(row=2,column=4).value = response_data[key]
            elif key == "vkn":
                sheet.cell(row=2,column=5).value = response_data[key]
            elif key == "tarih":
                sheet.cell(row=2,column=1).value = response_data[key]
            elif key == "fatura_Serisi":
                sheet.cell(row=2,column=2).value = response_data[key]
            elif key == "fatura_No":
                sheet.cell(row=2,column=3).value = response_data[key]
            elif "product" in key:
                line = int(key.split("_")[-1])
                product_list = response_data[key]
                sheet.cell(row=line+1,column=6).value = product_list[0]
                sheet.cell(row=line+1,column=7).value = product_list[1]
                sheet.cell(row=line+1,column=8).value = product_list[2]
                sheet.cell(row=line+1,column=9).value = product_list[3]

    if "http://www.pierrecardin.com.tr" in text: # pierrecardin
        for key in response_data:
            if key == "title":
                sheet.cell(row=2,column=4).value = response_data[key]
            elif key == "vkn":
                sheet.cell(row=2,column=5).value = response_data[key]
            elif key == "tarih":
                sheet.cell(row=2,column=1).value = response_data[key]
            elif key == "fatura_Serisi":
                sheet.cell(row=2,column=2).value = response_data[key]
            elif key == "fatura_No":
                sheet.cell(row=2,column=3).value = response_data[key]
            elif "product" in key:
                line = int(key.split("_")[-1])
                product_list = response_data[key]
                if len(product_list) > 5:
                    sheet.cell(row=line+1,column=6).value = product_list[1]
                    sheet.cell(row=line+1,column=7).value = product_list[3]
                    sheet.cell(row=line+1,column=8).value = product_list[4]
                    sheet.cell(row=line+1,column=9).value = product_list[5]
                elif len(product_list) == 5:
                    sheet.cell(row=line+1,column=6).value = "KARGO BEDELİ"
                    sheet.cell(row=line+1,column=7).value = product_list[0]
                    sheet.cell(row=line+1,column=8).value = product_list[1]
                    sheet.cell(row=line+1,column=9).value = product_list[2]

    return workbook